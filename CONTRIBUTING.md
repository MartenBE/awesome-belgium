# Contributions guidelines

Ensure your pull request adheres to the following guidelines:

* Categories are listed in alphabetical order. Links within a category are placed in alphabetical order ignoring any "`https://`" or "`https://www.`" prefixes (e.g. "`https://www.aaa.be`" comes before "`https://bbb.be`").
* Place links in the right category. New categories or improvements to the existing categorization are welcome, but should be done in a separate pull request.
* Links should start with "`https://`" and do not end on a slash ("`/`"). Links with the unsafe "`http://`" prefix will not be accepted. When possible, ommit the "`www.`" prefix.
* Links should always be reachable online (e.g. no 404 or unreachable websites).
* Links should point to up to date resources or organisations, so no news articles, ... which go out of date after a few months.
* Descriptions start with a capital and end with a full stop/period ("`.`").
* Descriptions are written in English. This allows to maintain a single description per link.
* Keep descriptions short and simple, but descriptive.
* Make sure the spelling/grammar of the description is correct.
* Make sure your text editor is set to remove trailing whitespace.
* The pull request should have a useful title and why the links should be included.

If you cannot create a pull request or have a question, you can create an issue.

Thank you for your contribution!