# Awesome Belgium

**Useful online resources for Belgian citizens.**

Contributions are welcome! Check the [contributing guidelines](./CONTRIBUTING.md) for more information.

---

* [Business and commerce](#business-and-commerce)
* [Calendar](#calendar)
* [Communities](#communities)
* [Concerts, festivals and events](#concerts-festivals-and-events)
* [Finances](#finances)
* [Geographical information](#geographical-information)
* [Governments](#governments)
* [Health (both mental and physical)](#health-both-mental-and-physical)
* [ICT](#ict)
* [Housing](#housing)
* [Jobs](#jobs)
* [Language](#language)
* [Leisure and hobbies](#leisure-and-hobbies)
* [Living](#living)
* [Nature](#nature)
* [News](#news)
* [People](#people)
* [Second hand buying and selling](#second-hand-buying-and-selling)
* [Shopping](#shopping)
* [Statistics](#statistics)
* [Transportation](#transportation)
* [Weather](#weather)

---

## Business and commerce

* https://goudengids.be - Find a professional or business.
* https://openingsurengids.be - Find the opening hours of a business.

## Calendar

* https://belgieschoolvakanties.be - Overview of school vacations, public holidays and construction leave.

## Communities

* https://hoplr.be - The social network for your neighbourhood.

## Concerts, festivals and events

* https://kinepolis.be - A Belgian cinema chain.
* https://www.livenation.be - Seller of tickets to various events.
* https://ticketmaster.be - Seller of tickets to various events.
* https://ticketswap.be - Marketplace to buy or safely and officially resell tickets to various events.
* https://uitinvlaanderen.be - A leisure agenda for Flanders and Brussels.

## Finances

* https://carbu.com/belgie/ - Compare fuel stations and fuel prices.
* https://myminfin.be - The platform of the FPS Finance on which you can manage your tax file, consult your personal documents and use Tax-on-web.
* https://vtest.vreg.be - Compare and find the energy contract that suits you. A test developped by the Flemish Regulator of the Electricity and Gas Market (VREG).

## Geographical information

* https://geopunt.be - The central access point for Flemmish geographical government information.
* https://geoportail.wallonie.be - The central access point for Walloon geographical government information.

## Governments

* https://belgium.be - Official information and services of the Belgian government.
* https://vlaanderen.be - Official information from the Flemish government.

## Health (both mental and physical)

* https://112.be - The European emergency number for ambulance, fire brigade and/or police.
* https://1722.be - The number when you need help from the firebrigade when no lives ar in danger and there is no fire (e.g. storm or flood damage).
* https://1712.be - Professional helpline for questions about violence, abuse, and child maltreatment.
* https://antigifcentrum.be - Belgian antitoxin center. You can call them in case of acute poisoning.
* https://apotheek.be - Need a pharmacy on the weekend or on holiday? Find the pharmacy on duty.
* https://awel.be - Awel listens to all children and youth with a question, a story, a problem.
* https://childfocus.be - Foundation for missing and sexually exploited children. Call the toll-free emergency number 116 000 to report a disappearance or a case of sexual exploitation.
* https://druglijn.be - The place to go for all your questions about booze, drugs, pills, gaming and gambling. For information, initial advice or for addresses for help and prevention. Anonymous, objective and without judging or condemning.
* https://igvm-iefh.belgium.be - A place to file complaints concerning discrimination based on gender. Also offers free legal advice and help.
* https://info-coronavirus.be - Official information about the Corona virus and measures in Belgium.
* https://politie.be - Official site of the police.
* https://stopchildporno.be - Anyone who happens to stumble upon sexual abuse images of minors can report them here anonymously.
* https://soleilmalin.be - All necessary information to stay safe in the sun (e.g. sunburns, heat strokes, ...).
* https://tabakstop.be -  A free service you can go to if you want help to quit smoking.
* https://tele-onthaal.be - A free and anonymous service offering a helping call to anyone looking for a listening ear or who is worried about something.
* https://veiligonline.be - An initiative to help parents in the media education of their children.
* https://www.watwat.be - Offers answers to questions teenagers face.
* https://www.wachtposten.be - Need a doctor on the weekend or on holiday? Find the doctor on duty (also see https://health.belgium.be/nl/gezondheid/organisatie-van-de-gezondheidszorg/dringende-hulpverlening/wachtdiensten)
* https://zelfmoord1813.be - A free and anonymous service for people thinking about suicide.

## ICT

* https://besttariff.be/ - A tool to find the best tariff for fixed telephony, mobile telephony and broadband internet provided by the Belgian Institute for Postal services and Telecommunications.
* https://safeonweb.be - Provides help with online scams, spam, phishing, data theft, ... and offers advise to stay safe on the internet.

## Housing

* https://immoweb.be - Buy, sell or rent houses, apartments, lands, garages, offices, shops, industries, new buildings.
* https://zimmo.be - Searches real estate on all real estate websites and bundles it in a user-friendly search engine.

## Jobs

* https://jobat.be - Search for job openings.
* https://vdab.be - The employment service of Flanders.

## Language

* https://namen.taalunie.org - Foreign geographic names in Dutch.
* https://nederlandsoefenen.be - A collection of websites for people who want to practice Dutch in Flanders or Brussels
* https://taaladvies.net - Answers to specific questions about language and spelling often asked of language advice services in the Netherlands and Belgium.
* https://vandale.be - Dutch dictionary.
* https://wallangues.be - Access to free French language training.
* https://woordenlijst.org - Glossary of the Dutch Language (popularly known as het Groene Boekje).

## Leisure and hobbies

* https://kampas.be - Website to connect (youth-)movements and schools with Flemish locations for multi-day stays.
* https://kwaliteitzwemwater.be - Map of places where you can swim indoors or outdoors, or go for watersports.

## Living

* https://living-in-belgium.be - Tips for living in Belgium.
* https://moving-tobelgium.be - Tips for moving to Belgium.

## Nature

* https://waarnemingen.be - The largest nature platform in the Netherlands and Belgium where everyone can share nature observations via the internet in order to document the richness of nature for now and for the future.

## News

* https://demorgen.be
* https://hln.be
* https://humo.be
* https://knack.be
* https://nieuwsblad.be
* https://reddit.com/r/belgium
* https://standaard.be
* https://tijd.be
* https://tweakers.net - Electronics and technology website of the Netherlands and Belgium.
* https://vrtnws.be - The news service of the VRT, the Flemish public broadcast. Visit https://vrtlite.be for a text-only version (no photos, videos, liveblogs, scripts, trackers and cookies).

## People

* https://1207.be - Find people.
* https://wittegids.be - Find people.

## Second hand buying and selling

* https://2dehands.be - Buy or sell any kind of used stuff.
* https://airsoftbazaar.be - Buy or sell used airsoft replicas and gear.
* https://autoscout24.be - Buy or sell new or used cars.
* https://www.detransformisten.be/agenda-repair-cafes - Find a repair cafe (a meeting in which people repair household electrical and mechanical devices, computers, bicycles, clothing, ...) in Flanders or Brussels.
* https://peerby.com/en-be - Borrow and rent things with your neighbors.

## Shopping

* https://home24.be - Large furniture store.
* https://standaardboekhandel.be - Large bookstore.

## Statistics

* https://statbel.fgov.be - the Belgian statistical office which collects, produces and disseminates reliable and relevant figures on the Belgian economy, society and territory.
* https://walstat.iweps.be - The portal for local statistical information on Wallonia.

## Transportation

* https://belgiantrain.be - Official site of the National Railway Company of Belgium.
* https://flits.bnet.be - Overview of traffic and speed cameras.
* https://skyscanner.be - Find the cheapest airplane tickets.
* https://verkeerscentrum.be - A live map regarding traffic jams and accidents on Flemmish roads.

## Weather

* https://buienradar.be - Map with rain predicitions.
* https://meteovista.be - Weather information.
* https://warmedagen.be - Hot weather tips.
